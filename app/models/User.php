<?php

namespace App\Models;

use PDO;
use Core\Model;

require_once '../core/Model.php';
/**
*
*/
class User extends Model
{
    function __construct()
    {

    }

    public static function all()
    {
        $db = User::db();
        $statement = $db->query('SELECT * FROM users');
        $users = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $users;
    }

    public function paginate($size = 10)
    {
        if (isset($_REQUEST['page'])){
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = User::db();
        $statement = $db->prepare('SELECT * FROM users LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $users = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $users;
    }

    public static function rowCount()
    {
        $db = User::db();
        $statement = $db->prepare('SELECT count(id) AS count FROM users');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);

        return $rowCount['count'];
    }

    public static function find($id)
    {
        $db = User::db();
        $statement = $db->prepare('SELECT * FROM users WHERE id=:id');
        $statement->execute(array(':id' => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, User::class);
        $user = $statement->fetch(PDO::FETCH_CLASS);
        return $user;
    }

    public function insert()
    {
        $db = User::db();
        $statement = $db->prepare('INSERT INTO users(name, surname, age, email) VALUES(:name, :surname, :age, :email)');
        $statement->bindValue(':name', $this->name);
        $statement->bindValue(':surname', $this->surname);
        $statement->bindValue(':age', $this->age);
        $statement->bindValue(':email', $this->email);
        return $statement->execute();
    }

    public function delete()
    {
        $db = User::db();
        $statement = $db->prepare('DELETE FROM users WHERE id = :id');
        $statement->bindValue(':id', $this->id);
        return $statement->execute();
    }

    public function save()
    {
        $db = User::db();
        $statement = $db->prepare('UPDATE users SET name = :name, surname = :surname, age = :age, email = :email WHERE id = :id');
        var_dump($_REQUEST);
        $statement->bindValue(':id', $this->id);
        $statement->bindValue(':name', $this->name);
        $statement->bindValue(':surname', $this->surname);
        $statement->bindValue(':age', $this->age);
        $statement->bindValue(':email', $this->email);
        return $statement->execute();
    }

    public function setPassword($password)
    {
        $password = password_hash($password, PASSWORD_BCRYPT);
        $db = User::db();
        $statement = $db->prepare('UPDATE users SET password = :password WHERE id = :id');
        $statement->bindValue(':id', $this->id);
        $statement->bindValue(':password', $password) ;
        $statement->execute();
        return $password;
    }

    public function passwordVerify($password)
    {
        return password_verify($password, $this->password);
    }
}
