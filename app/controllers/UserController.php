<?php
namespace App\Controllers;

require_once  '../app/models/User.php';

use \App\Models\User;
/**
*
*/
class UserController
{

    function __construct()
    {

    }

    public function index()
    {
        $pagesize = 2;
        $users = User::paginate($pagesize);
        $rowCount = User::rowCount();
        //$users = User::all();
        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])){
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }
        require "../app/views/user/index.php";
    }

    public function show($arguments)
    {
        $id = (integer) $arguments[0];
        $user = User::find($id);
        require "../app/views/user/show.php";
    }

    public function create()
    {
        require "../app/views/user/create.php";
    }

    public function store()
    {
        $user = new User();
        $user->name = $_REQUEST['name'];
        $user->surname = $_REQUEST['surname'];
        $user->age = $_REQUEST['age'];
        $user->email = $_REQUEST['email'];
        $user->insert();
        header('Location:/user');
    }

    public function delete($arguments)
    {
        $id = (integer) $arguments[0];
        $user = User::find($id);
        $user->delete();
        header('Location:/user');
    }

    public function edit($arguments)
    {
        $id = (int) $arguments[0];
        $user = User::find($id);
        require '../app/views/user/edit.php';
    }

    public function update()
    {
        $id = $_REQUEST['id'];
        $user = User::find($id);
        $user->name = $_REQUEST['name'];
        $user->surname = $_REQUEST['surname'];
        $user->age = $_REQUEST['age'];
        $user->email = $_REQUEST['email'];
        $user->save();
        header('Location:/user');
    }
}
